#!/bin/bash
## Lab 7
##
## With lots of help from #88 Acey Deucy game from Wicked Cool Shell Scripts
## The Acey Duecy game from the book already has code for
## the deck of 52 playing cards. The last part of the code from the book
## is how the original game's ruleset actually plays out for the user.
## Acey Deucy is where you would have to guess if the
##	3rd card drawed is between the rank value of
##	the 1st and 2nd card drawed.
##	You win if you guess the 3rd card is between the 1st and 2nd card values.
##	You lose if the 3rd card happens not to be in the middle of the 
##	1st and 2nd card values.
##
## I changed that last part of the code to make a different game.
## The game I made is a number card game. It works differently than Acey Deucy
## because now there will be 4 different cards in play.
## 2 cards for the player and 2 cards for the dealer.
## The goal of this game is for the player to guess
##	 if their card value total is either higher or lower
##	 than the dealer's hand total.

## function to make deck of cards
function makeDeck {
	#Creation for deck of 52 cards
	card=1
	while [ $card -le 52 ]
	do
		deck[$card]=$card
		card=$(( $card + 1 ))
	done
}

## function to randomize the deck of cards
function shuffleDeck {
	#Randomize the deck of cards and make sure the deck does not have 53 cards
	count=1
	while [ $count != 53 ]
	do
		drawCard	## call drawCard function
		newdeck[$count]=$picked
		count=$(( $count + 1 ))
	done
}

## function to draw a random card from the deck
function drawCard {
	#Draw a random card from the deck
	local errcount randomcard
	threshold=10
	errcount=0

	while [ $errcount -lt $threshold ]
	do
		randomcard=$(( ( $RANDOM % 52 ) + 1 ))
		errcount=$(( $errcount + 1 ))

		if [ ${deck[$randomcard]} -ne 0 ] ; then
			picked=${deck[$randomcard]}
			deck[$picked]=0
			return $picked
		fi
	done

	randomcard=1

	while [ ${newdeck[$randomcard]} -eq 0 ]
	do
		randomcard=$(( $randomcard + 1 ))
	done

	picked=$randomcard
	deck[$picked]=0

	return $picked
}

## function that shows the card's rank and suit
function showCard {
card=$1

## If the randomizer makes the card value greater than 52
## 	then the game will automatically quit.
if [ $card -lt 1 -o $card -gt 52 ] ; then
	echo " "
	echo "Bad card value of $card, time to quit"
	exit 1
fi

suit="$(( ( ( $card - 1 ) / 13 ) + 1))"
rank="$(( $card % 13))"

case $suit in
	1 ) suit="Hearts" ;;
	2 ) suit="Clubs" ;;
	3 ) suit="Spades" ;;
	4 ) suit="Diamonds" ;;
	* ) echo "Bad suit value: $suit"
		exit 1
esac

case $rank in
	0  ) rank="King" ;;
	1  ) rank="Ace" ;;
	11 ) rank="Jack" ;;
	12 ) rank="Queen" ;;
esac

cardname="$rank of $suit"
}

## Function on how the cards will be dealt
function dealCards {
	
	card1=${newdeck[1]}
	card2=${newdeck[2]}
	card3=${newdeck[3]}
	card4=${newdeck[4]}

	rank1=$(( ${newdeck[1]} % 13 ))
	rank2=$(( ${newdeck[2]} % 13 ))
	rank3=$(( ${newdeck[3]} % 13 ))
	rank4=$(( ${newdeck[4]} % 13 ))

	if [ $rank1 -eq 0 ] ; then
		rank1=13;
	fi

	if [ $rank2 -eq 0 ] ; then
                rank2=13;
        fi
	
	if [ $rank3 -eq 0 ] ; then
                rank3=13;
        fi
	
	if [ $rank4 -eq 0 ] ; then
                rank4=13;
        fi

	if [ $rank1 -gt $rank2 ] ; then
		temp=$card; card1=$card2; card2=$temp
		temp=$rank1; rank1=$rank2; rank2=$temp
	fi

	## Call the showCard function
	showCard $card1 ; cardname1=$cardname
	showCard $card2 ; cardname2=$cardname

	showCard $card3 ; cardname3=$cardname
	showCard $card4 ; cardname4=$cardname

	echo "You draw:" ; echo "	$cardname1" ; echo "	$cardname2"

}

games=0
won=0

while [ /bin/true ] ; do
	# Call the functions for making the deck, randomizing the deck, and dealing cards.
	makeDeck
	shuffleDeck
	dealCards

	# Variables to hold total card value for the player and dealer
	addMyHand=$(( $rank1 + $rank2 ))
	addDealer=$(( $rank3 + $rank4 ))

	# Tell the player their cards and what they would like to guess.
	/bin/echo -n "Your hand total is $addMyHand."
	echo " "
	/bin/echo -n "Do you think your hand will be higher or lower than the dealer?"
	echo " "
	/bin/echo -n "Please choose between (high/low/check): "
	read answer
	# Checks the number of times the player has won the game
	if [ "$answer" = "check" ] ; then
		echo ""
		echo "You played $games games and won $won times."
		exit 0
	fi
	# Tells the player what the dealer's hand is after the player has made their guess.
	echo "Dealer draws: $cardname3 and $cardname4 which totals up to $addDealer"
	
	# How the game works:
	# Player hand guess greater than dealer hand = win
	# Player hand guess less than dealer hand = win
	# Player guesses wrong = lose
	if [ $addMyHand -gt $addDealer ] 
       	then       
		winner=1 
	elif [ $addMyHand -lt $addDealer ] 
       	then       
		winner=1 
	else
		winner=0
	fi

	# Prompt the player their results if they win or lose
	# by taking their guess and showing the results of it.
	if [ $winner -eq 1 -a "$answer" = "high" ] ; then
		echo "Your hand of $addMyHand is higher than the dealer's $addDealer."
		echo "You bet that it would be higher, YOU WIN!"
		won=$(( $won + 1 ))
	elif [ $winner -eq 0 -a "$answer" = "low" ] ; then
		echo "Your hand of $addMyHand is lower than the dealer's $addDealer."
		echo "You bet that it would be lower, YOU WIN!"
		won=$(( $won + 1 ))
	else
		echo "You made the wrong bet. You lose."
	fi

	games=$(( $games + 1 )) # How many times you played the game.

done

exit 0
